<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <table class="table table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Product Price</th>
        <th>Product brand name</th>
        <th>category</th>
        <th colspan=3>Action</th>
      </tr>
    </thead>

    @php
      $i=1;
    @endphp
    @foreach($products as $product)
      <tr>
        <td>@php echo $i++; @endphp</td>
        <td>{{$product->code}}</td>
        <td>{{$product->name}}</td>
        <td>{{$product->price}}</td>
        <td>{{$product->brands->name}}</td>
        <td> @foreach($product->categories as $c) {{$c->name}} @endforeach<br></td>
        <td><a href="{{action('ProductController@edit', $product->id)}}" class="btn btn-warning">Edit</a></td>&nbsp;
        <td><a href="{{action('ProductController@show', $product->id)}}" class="btn btn-info">Details</a></td>&nbsp;
          <form action="{{action('ProductController@destroy', $product['id'])}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <td><button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure?')">Delete</button></td>
            @endforeach
          </form>
        </td>
      </td>
    </tr>
  </table>

  </body>
</html>
