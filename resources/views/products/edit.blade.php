<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    <div class="container">
      <h2>Edit Product Information</h2>
      <br />
      <form method="post" action="{{action('ProductController@update', $return_product->id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Code:</label>
            <input type="text" class="form-control" name="code" value="{{$return_product->code}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" value="{{$return_product->name}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="email">Description</label>
            <input type="text" class="form-control" name="desc" value="{{$return_product->description}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <lable>Brand: </lable>
            <select name="brand_id" class="form-control" value="{{$return_product->brand_id}}">
              @foreach($brands as $b)
              <option value="{{$b->id }}">{{ $b->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <lable>Category:
            </lable>
            <br>
            <select name="category_id[]" class="form-control" multiple="true">
              @foreach($categories as $category)
              @if (in_array($category->id,$product_categories))
                <option value="{{$category->id }}" class="category-opt" selected>{{ $category->name }}</option>
              @else
                <option value="{{$category->id }}" class="category-opt">{{ $category->name }}</option>
              @endif
              @endforeach
              </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>


<script>
  $(document).ready(function(){
    var arr="{{$return_product}}";
    console.log(arr[0])
  })
</script>
