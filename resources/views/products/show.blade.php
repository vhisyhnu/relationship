<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    <div class="container">
      <h2>Show Product Information</h2>
      <br />
      <form method="post" action="{{action('ProductController@update', $return_product->id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Code:</label>
            <input type="text" class="form-control" name="code" value="{{$return_product->code}}"readonly>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" value="{{$return_product->name}}"readonly>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="email">Description</label>
            <input type="text" class="form-control" name="desc" value="{{$return_product->description}}"readonly>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="email">Price</label>
            <input type="text" class="form-control" name="desc" value="{{$return_product->price}}"readonly>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <lable>Brand: </lable>
            <select name="brand_id" class="form-control" value="{{$return_product->brand_id}}"readonly>
              @foreach($brands as $b)
              <option value="{{$b->id }}">{{ $b->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <lable>Category:
            </lable>
            <br>
            <select name="category_id[]" class="form-control" multiple="true"readonly>
              @foreach($categories as $category)
              @if (in_array($category->id,$product_categories))
                <option value="{{$category->id }}" class="category-opt" selected>{{ $category->name }}</option>
              @else
                <option value="{{$category->id }}" class="category-opt">{{ $category->name }}</option>
              @endif
              @endforeach
              </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <a href="{{action('ProductController@edit', $return_product->id)}}" class="btn btn-outline-secondary">Edit</a>
            &nbsp;
            <a href="/products" class="btn btn-outline-primary">Back</a>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>


<script>
  $(document).ready(function(){
    var arr="{{$return_product}}";
    console.log(arr[0])
  })
</script>
