<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Add New Product</h2>
      <br/>
      <form method="post" action="{{url('products')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="Name">Code:</label>
             <input type="text" class="form-control" name="code" placeholder="e.g. 10001">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="Name">Name:
             </label>
             <input type="text" class="form-control" name="name" placeholder="e.g. Keropok Pisang Besar">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4"></div>
           <div class="form-group col-md-4">
             <label for="Name">Description: </label>
             <input type="text" class="form-control" name="desc" placeholder="e.g. Keropok pisang pek 500gm">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="Name">Price: (RM)</label>
             <input type="text" class="form-control" name="price" placeholder="e.g. 10.00" >
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <lable>Brand:
             </lable>
             <select name="brand_id" class="form-control">
               @foreach($brands as $b)
               <option value="{{ $b->id }}">{{ $b->name }}
               </option>
               @endforeach
             </select>
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <lable>Category:
             </lable>
             <select name="category_id[]" class="form-control" multiple="true">
               @foreach($categories as $c)
               <option value="{{ $c->id }}">{{ $c->name }}
               </option>
               @endforeach
             </select>
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4" style="margin-top:60px">
             <button type="submit" class="btn btn-success">Submit
             </button>
           </div>
         </div>
       </form>
     </div>
    <body>
