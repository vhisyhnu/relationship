<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Add New Brand</h2>
      <br/>
      <form method="post" action="{{url('brands')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="Name">Name:</label>
             <input type="text" class="form-control" name="name" placeholder="e.g. Prius">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="Name">Company:
             </label>
             <input type="text" class="form-control" name="company" placeholder="e.g. Toyota">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4" style="margin-top:60px">
             <button type="submit" class="btn btn-success">Submit
             </button>
           </div>
         </div>
       </form>
     </div>
    <body>
