<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <table class="table table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Brand Name</th>
        <th>Company</th>
        <th colspan=3>Action</th>
      </tr>
    </thead>

    @php
      $i=1;
    @endphp
    @foreach($brands as $brand)
      <tr>
        <td>@php echo $i++; @endphp</td>
        <td>{{$brand->name}}</td>
        <td>{{$brand->company}}</td>
        <td><a href="{{action('BrandController@edit', $brand->id)}}" class="btn btn-warning">Edit</a></td>&nbsp;
        <td><a href="{{action('BrandController@show', $brand->id)}}" class="btn btn-info">Details</a></td>&nbsp;
          <form action="{{action('BrandController@destroy', $brand->id)}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <td><button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure?')">Delete</button></td>
            @endforeach
          </form>
        </td>
      </td>
    </tr>
  </table>

  </body>
</html>
