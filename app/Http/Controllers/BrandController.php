<?php

namespace App\Http\Controllers;

use App\brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $brands = \App\Brand::all();
      return view('brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $brand= new \App\Brand;
      $brand->name=$request->get('name');
      $brand->company=$request->get('company');
      $brand->save();
      return redirect('brands')->with('success', 'Brands has been added');//

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $brand = Brand::where('id',$id)->firstOrFail();  //
      return view('brands.show',compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $brand = Brand::where('id',$id)->firstOrFail();
      return view('brands.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $brand= \App\Brand::find($id);
      $brand->name=$request->get('name');
      $brand->company=$request->get('company');
      $brand->save();
      return redirect('brands')->with('success', 'Product has been updated');//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(brand $brand)
    {
      $name = $brand->name;
      $brand->delete();
      return redirect('brands')->with('success','Brand '.$name.' has been deleted');
    }
}
