<?php

namespace App\Http\Controllers;

use App\Product;
use App\Brand;
use App\Category;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('brands','categories')->orderBy('name','asc')->get();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $brands = Brand::get(); // for dropdown menu
      $categories = Category::get();// for dropdown menu
      return view('products.create',compact('brands','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $product= new \App\Product;
      $product->code=$request->get('code');
      $product->name=$request->get('name');
      $product->description=$request->get('desc');
      $product->price=$request->get('price');
      $product->brand_id=$request->get('brand_id');
      $product->save();
      $cats = $request->input('category_id');
      foreach($cats as $c)
      {
        $categories = Category::find([$c]);
        $product->categories()->attach($categories);
      }
      return redirect('products')->with('success', 'Product has been added');//
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $brands = Brand::get();
      $categories = Category::get();
      $return_product=Product::where('id',$id)->firstOrFail();
      $product_categories=[];
      $product_categories_raw=$return_product->categories;
      forEach($product_categories_raw as $category){
        array_push($product_categories,$category->id);
      }
      return view ('products.show',compact('return_product',"categories","brands","product_categories"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    /*public function edit(product $product)
    {
      $brands = Brand::get(); // for dropdown menu
      $categories = Category::get();// for dropdown menu
      $return_product =$product;
      $product_category=$product->categories->id->get();
      return view('products.edit',compact('return_product','product_category','categories','brands'));
    }*/

    public function edit($id){
      $brands = Brand::get();
      $categories = Category::get();
      $return_product=Product::where('id',$id)->firstOrFail();
      $product_categories=[];
      $product_categories_raw=$return_product->categories;
      forEach($product_categories_raw as $category){
        array_push($product_categories,$category->id);
      }
      return view ('products.edit',compact('return_product',"categories","brands","product_categories"));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      {
        $product= \App\Product::find($id);
        $product->code=$request->get('code');
        $product->name=$request->get('name');
        $product->description=$request->get('desc');
        $product->brand_id=$request->get('brand_id');

        $product->save();
        //many to many
        $product->categories()->detach();
        // drop all existing relationship
        $cats = $request->input('category_id');
        foreach ($cats as $c)
        {
          $categories = Category::find([$c]);
          $product->categories()->attach($categories);
        }

        return redirect('products')->with('success', 'Product has been updated'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(product $product)
    {
      {
          $name = $product->name;
          $product->categories()->detach();
          // drop all existing relationship
          $product->delete();
          return redirect('products')->with('success','Product '.$name.' has been deleted');
        }

    }
}
