<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
  public function brands()
  {
    return $this->belongsTo('App\Brand','brand_id');
  }
  public function categories()
  {
    return $this->belongsToMany('App\Category');
  }
}
